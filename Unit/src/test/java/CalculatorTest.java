import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculatorTest {
    @DataProvider(name = "testDataCalculate")
    public Object[][] testDataCalculate() {
        return new Object[][]
                {
                        {"1", "2", 3, "1"},
                        {"1", "1", 0, "2"},
                        {"2", "1", 2, "3"},
                        {"2", "2", 1, "4"},
                        {"0", "0", 0, "1"},
                        {"0", "1", 1, "1"},
                        {"-1", "1", 0, "1"},
                        {"123", "1", 122, "2"},
                        {"4", "-1", 5, "2"},
                        {"-22", "3", -66, "3"}
                };
    }

    @DataProvider(name = "testDataInvalidCalculate")
    public Object[][] testDataInvalidCalculate() {
        return new Object[][]
                {
                        {"3", "xyt", "1"},
                        {"tyrt", "2", "2"},
                        {"2.3", "3.1", "5.4"},
                        {"4.8", "2.2", "8.1"},
                        {"@", "!", "("},
                        {"*", "%", ")"},
                        {"0.33333", "4.333", "1.993"}
                };
    }

    @DataProvider(name = "testDataInvalidOperation")
    public Object[][] testDataInvalidOperation() {
        return new Object[][]
                {
                        {"1", "1", "5"},
                        {"3", "2", "6"}

                };
    }

    @DataProvider(name = "testDataZero")
    public Object[][] testDataZero() {
        return new Object[][]
                {
                        {1, 0},
                        {10, 0},
                        {100, 0},
                        {0, 0},
                        {-1, 0}

                };
    }

    @Test(dataProvider = "testDataCalculate")
    public void calculateTest(String first, String second, int expected, String operation) {
        Calculator calculator = new Calculator();
        Assert.assertEquals(calculator.calculate(first, second, operation), expected);
    }

    @Test(dataProvider = "testDataInvalidCalculate", expectedExceptions = NumberFormatException.class)
    public void calculateStringTest(String first, String second, String operation) {
        Calculator calculator = new Calculator();
        calculator.calculate(first, second, operation);
    }


    @Test(dataProvider = "testDataZero", expectedExceptions = ArithmeticException.class)
    public void testZero(int first, int second) {
        Calculator calculator = new Calculator();
        calculator.division(first, second);
    }

    @Test(dataProvider = "testDataInvalidOperation", expectedExceptions = IllegalArgumentException.class)
    public void testOperation(String first, String second, String operation) {
        Calculator calculator = new Calculator();
        calculator.calculate(first, second, operation);
    }
}