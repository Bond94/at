import com.sun.javaws.exceptions.InvalidArgumentException;

public class Calculator {
    public int sum(int first, int second) {
        return first + second;
    }

    public int difference(int first, int second) {
        return first - second;
    }

    public int multiplication(int first, int second) {
        return first * second;
    }

    public int division(int first, int second) {
        if (second == 0) {
            throw new ArithmeticException("на ноль делить нельзя");
        }

        return first / second;
    }

    public int calculate(String first, String second, String operation ) {
        int firstNumber = Integer.parseInt(first);
        int secondNumber = Integer.parseInt(second);

        switch (operation) {
            case "1":
                return sum(firstNumber, secondNumber);
            case "2":
                return difference(firstNumber, secondNumber);
            case "3":
                return multiplication(firstNumber, secondNumber);
            case "4":
                return division(firstNumber, secondNumber);
            default:
                throw new IllegalArgumentException();
        }
    }
}
