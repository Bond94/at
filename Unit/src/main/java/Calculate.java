import java.util.Scanner;

public class Calculate {
    public static void main(String[] args) {
        String first;
        String second;
        String operation;

        Calculator calculator = new Calculator();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите номер операции которую хотите сделать: 1 - сложение, 2 - вычитание, 3 - умножение, 4 - деление");
        operation = scanner.nextLine();
        System.out.println("-------------------------------------");
        System.out.println("Введите 1 значение");
        first = scanner.nextLine();
        System.out.println("Введите 2 значение");
        second = scanner.nextLine();

        System.out.println("-------------------------------------");
        System.out.println("Результат равен: " + calculator.calculate(first, second, operation));
    }
}
