public class Student {
    String fullname;
    int age;
    int course;
    int avg;

    public Student(String fullname, int age, int course, int avg) {
        this.fullname = fullname;
        this.age = age;
        this.course = course;
        this.avg = avg;
    }

    @Override
    public String toString() {
        return "Student{" +
                "fullname='" + fullname + '\'' +
                ", age=" + age +
                ", course=" + course +
                ", avg=" + avg +
                '}';
    }
}
