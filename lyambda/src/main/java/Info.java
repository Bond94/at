import java.util.ArrayList;

public class Info {
    public static void main(String[] args) {
        Student student1 = new Student("Sergey Ivanov", 32, 2, 8);
        Student student2 = new Student("Petr Markov", 31, 3, 2);
        Student student3 = new Student("Oleg Vidov", 35, 1, 5);
        Student student4 = new Student("Olga Stupak", 27, 4, 9);
        Student student5 = new Student("Igor Liber", 22, 4, 9);

        ArrayList<Student> list = new ArrayList<>();
        list.add(student1);
        list.add(student2);
        list.add(student4);
        list.add(student3);
        list.add(student5);

        Info info = new Info();
        info.testCheck(list, (Student s) -> s.age < 30);

    }

    interface Check {
        boolean check(Student s);
    }

    void testCheck(ArrayList<Student> st, Check check) {
        for (Student s: st) {
            if (check.check(s)) {
                System.out.println(s);
            }
        }
    }
}
