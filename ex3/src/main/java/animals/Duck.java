package animals;

public class Duck extends Herbivore implements Fly, Swim, Voice {

    public Duck(String name, Size size) {
        super(name, size);
    }

    @Override
    public void fly() {
        System.out.println("Летай утка");
    }

    @Override
    public void swim() {
        System.out.println("Плавай утка");
    }

    @Override
    public void voice() {
        System.out.println("Утка говорит Га-га-га");
    }
}
