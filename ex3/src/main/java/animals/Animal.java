package animals;

import food.Food;

import java.util.Objects;

public abstract class Animal {

    public String name;
    public Size size;

    public String getName() {
        return name;
    }

    public Size getSize() {
        return size;
    }

    public Animal(String name, Size size) {
        this.name = name;
        this.size = size;
    }

    public abstract void eat(Food food) throws WrongFoodException;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Animal animal = (Animal) obj;
        return Objects.equals(this.name, animal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }


}
