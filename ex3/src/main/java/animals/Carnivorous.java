package animals;

import food.Food;
import food.Grass;


public abstract class Carnivorous extends Animal {

    public Carnivorous(String name, Size size) {
        super(name, size);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if (food instanceof Grass) {
            throw new WrongFoodException();
        }

        System.out.println(name + " ест мясо");
    }


}
