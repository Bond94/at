package animals;

public class Giraffe extends Herbivore implements Run, Voice {

    public Giraffe(String name, Size size) {
        super(name, size);
    }

    @Override
    public void run() {
        System.out.println("Бегай жираф");
    }

    @Override
    public void voice() {
        System.out.println("Жираф шипит");
    }
}
