package animals;


import food.Food;
import food.Meat;

public abstract class Herbivore extends Animal {

    public Herbivore(String name, Size size) {
        super(name, size);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if (food instanceof Meat) {
            throw new WrongFoodException();
        } else {
            System.out.println(name + " ест траву");
        }
    }


}
