package animals;

public enum Size {
    SMALL,
    MEDIUM,
    HIGH,
    HUGE
}
