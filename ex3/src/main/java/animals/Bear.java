package animals;

import food.Food;
import food.Meat;

public class Bear extends Carnivorous implements Run, Voice, Satiety {
    public int satiety= 5;

    public Bear(String name, Size size) {
        super(name, size);
    }

    @Override
    public void run() {
        System.out.println("Бегай медведь");
        satiety--;
    }

    @Override
    public void voice() {
        System.out.println("Медведь издает звук рыка");
    }


    @Override
    public void getSatiety() {
        System.out.println("Сытость медведя " + satiety);

    }
}
