package animals;

public class Wolf extends Carnivorous implements Run, Voice {

    public Wolf(String name, Size size) {
        super(name, size);
    }

    @Override
    public void run() {
        System.out.println("Бегай волк");
    }

    @Override
    public void voice() {
        System.out.println("Волк издает звук воя");
    }
}
