import animals.*;
import food.Food;
import food.Grass;
import food.Meat;

public class Zoo {
    public static void main(String[] args) {
        Food meat = new Meat();
        Food grass = new Grass();
        Worker worker = new Worker();
        Bear bear = new Bear("Миша", Size.HIGH);
        Duck duck = new Duck("Гриня", Size.SMALL);
        Fish fish = new Fish("Вася", Size.SMALL);
        Fox fox = new Fox("Алиса", Size.MEDIUM);
        Giraffe giraffe = new Giraffe("Игорь", Size.HUGE);
        Wolf wolf = new Wolf("Серый", Size.HIGH);


        worker.feed(bear, meat);
        worker.feed(bear, grass);
        bear.run();

        worker.getSatiety(bear);
        worker.feed(duck, meat);
        worker.feed(duck, grass);
        worker.feed(fish, meat);
        worker.feed(fish, grass);
        worker.feed(fox, meat);
        worker.feed(fox, grass);
        worker.feed(giraffe, meat);
        worker.feed(giraffe, grass);
        worker.feed(wolf, meat);
        worker.feed(wolf, grass);
        worker.getVoice(bear);
        worker.getVoice(duck);
        worker.getVoice(fox);
        worker.getVoice(giraffe);
        worker.getVoice(wolf);


        Swim[] pond = {duck, fish};

        for (int i = 0; i < pond.length; i++) {
            pond[i].swim();
        }

        Aviary<Herbivore> herbivore = new Aviary(Size.HUGE);
        herbivore.addAnimal(duck);
        herbivore.addAnimal(fish);
        herbivore.deleteAnimal(fish);
        herbivore.addAnimal(giraffe);

        Aviary<Carnivorous> carnivorous = new Aviary(Size.HIGH);
        carnivorous.addAnimal(bear);
        carnivorous.addAnimal(fox);
        carnivorous.addAnimal(wolf);

        System.out.println("Количество плотоядных животных в вольере " + carnivorous.numberAnimals());
        System.out.println("Количество травоядных животных в вольере " + herbivore.numberAnimals());


    }
}
