import animals.Animal;
import animals.Satiety;
import animals.Voice;
import animals.WrongFoodException;
import food.Food;

public class Worker {

    public void feed(Animal animal, Food food) {
        try {
            animal.eat(food);
        } catch (WrongFoodException e) {
            System.out.println(animal.getName() + " " + food.getFood());
        }
    }

    public void getVoice(Voice voice) {
        voice.voice();
    }

    public void getSatiety(Satiety satiety) {
        satiety.getSatiety();
    }


}
