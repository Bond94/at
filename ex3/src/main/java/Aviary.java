import animals.Animal;
import animals.Size;

import java.util.HashSet;
import java.util.Set;

public class Aviary<E extends Animal> {

    private Set<E> set = new HashSet<>();
    private Size size;

    public Aviary(Size size) {
        this.size = size;
    }

    public void addAnimal(E animal) {
        if (animal.getSize().compareTo(this.size) <= 0) {
            set.add(animal);
            System.out.println(animal.getName() + " поместился в размер");

        } else {
            System.out.println("Для животного " + animal.getName() + " вольер мал");
        }

    }

    public void deleteAnimal(E animal) {
        this.set.remove(animal);
    }

    public int numberAnimals() {
        return set.size();
    }


}
