package model;

public class Kotik {
    private int satiety = 2;
    private int prettiness;
    private String name;
    private int weight;
    private String meow;
    private int counter;

    public Kotik() {
        counter++;
    }

    public Kotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
        counter++;

    }

    public void setKotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }

    public int getCounter() {
        return counter;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public String getMeow() {
        return meow;
    }

    public int getSatiety() {
        return satiety;
    }

    public int eat() {
        return eat(satiety, "Борщ");
    }

    public int eat(int satiety) {
        satiety++;
        System.out.println("Котимк имеет сытость " + satiety);
        return satiety;
    }

    public int eat(int satiety, String eat) {
        satiety++;
        System.out.println("Котик имеет сытость " + satiety + ",потому что он поел " + eat);
        return satiety;
    }

    public boolean play() {
        if (satiety <= 0) {
            return false;
        }
        System.out.println("Play");
        satiety--;
        return true;
    }

    public boolean sleep() {

        if (satiety < 0) {
            return false;
        }
        System.out.println("Sleep");
        satiety--;
        return true;
    }


    public boolean chaseMouse() {
        if (satiety < 0) {
            return false;
        }

        System.out.println("Chase Mouse");
        satiety--;
        return true;
    }


    public boolean purrs() {

        if (satiety < 0) {
            return false;
        }
        System.out.println("Purrs");
        satiety--;
        return true;
    }

    public boolean sick() {

        if (satiety < 0) {
            return false;
        }
        System.out.println("Sick");
        satiety--;
        return true;
    }
    
    public void liveAnotherDay(int satiety) {
        for (int i = 0; i < 24; i++) {
            switch (1 + (int) (Math.random() * 5)) {
                case 1:
                    if (!play()) {
                        eat();
                    }
                    break;
                case 2:
                    if (!sleep()) {
                        eat(1);
                    }
                    break;
                case 3:
                    if (!chaseMouse()) {
                        eat();
                    }
                    break;
                case 4:
                    if (!purrs()) {
                        eat(1, "Мясо");
                    }
                    break;
                case 5:
                    if (!sick()) {
                        eat();
                    }
                    break;
            }
        }
    }


}
