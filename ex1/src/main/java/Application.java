import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik kotik = new Kotik();
        kotik.setKotik(3, "Tom", 3500, "Мяу");


        Kotik kotik1 = new Kotik(4, "John", 4000, "Мяу");

        kotik.liveAnotherDay(kotik.getSatiety());
        System.out.println("Котика зовут " + kotik.getName() + ",а его вес " + kotik.getWeight());

        //Вывести на экран результат сравнения одинаково ли разговаривают котики (сравнить переменные meow).
        if (kotik.getMeow().equals(kotik1.getMeow())) {
            System.out.println("Котики разговаривают одинаково");
        }

        System.out.println("Количество котиков " + (kotik.getCounter() + kotik1.getCounter()));


    }
}
