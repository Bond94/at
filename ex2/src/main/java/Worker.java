import animals.Animal;
import animals.Carnivorous;
import animals.Satiety;
import animals.Voice;
import food.Food;

public class Worker {

    public void feed(Animal animal, Food food) {
        animal.eat(food);


    }

    public void getVoice(Voice voice) {
        voice.voice();
    }

    public void getSatiety(Satiety satiety) {
        satiety.getSatiety();
    }


}
