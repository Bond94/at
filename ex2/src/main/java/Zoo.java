import animals.*;
import food.Food;
import food.Grass;
import food.Meat;

public class Zoo {
    public static void main(String[] args) {
        Animal animal = new Herbivore();
        Food meat = new Meat();
        Food grass = new Grass();
        Worker worker = new Worker();
        Bear bear = new Bear();
        Duck duck = new Duck();
        Fish fish = new Fish();
        Fox fox = new Fox();
        Giraffe giraffe = new Giraffe();
        Wolf wolf = new Wolf();




        worker.feed(bear, meat);
        worker.feed(bear, grass);
        bear.run();
        bear.eat(meat);
        // сытость медведя
        worker.getSatiety(bear);
        worker.feed(duck, meat);
        worker.feed(duck, grass);
        worker.feed(fish, meat);
        worker.feed(fish, grass);
        worker.feed(fox, meat);
        worker.feed(fox, grass);
        worker.feed(giraffe, meat);
        worker.feed(giraffe, grass);
        worker.feed(wolf, meat);
        worker.feed(wolf, grass);
        worker.getVoice(bear);
        worker.getVoice(duck);
        worker.getVoice(fox);
        worker.getVoice(giraffe);
        worker.getVoice(wolf);


        Swim[] pond = {duck, fish};

        for (int i = 0; i < pond.length; i++) {
            pond[i].swim();

            //System.out.println(pond[i].swim());

        }





//        bear.run();
//        bear.eat(food);
//        bear.voice("ревет");
//
//        Duck duck = new Duck();
//        duck.fly();
//        duck.swim();
//        duck.voice("га-га");
//        duck.eat(food);
//
//        Fish fish = new Fish();
//        fish.swim();
//        fish.eat(food);
//
//        Fox fox = new Fox();
//        fox.run();
//        fox.eat(food);
//        fox.voice("тяф-тяф");
//
//
//        Giraffe giraffe = new Giraffe();
//        giraffe.run();
//        giraffe.eat(food);
//        giraffe.voice("говори");
//
//        Wolf wolf = new Wolf();
//        wolf.run();
//        wolf.eat(food);
//        wolf.voice("воет");
//
    }
}
