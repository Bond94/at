package animals;

public class Fox extends Carnivorous implements Run, Voice {

    @Override
    public void run() {
        System.out.println("Бегай лиса");

    }

    @Override
    public void voice() {
        System.out.println("Лиса тявкает");
    }
}
