package animals;


import food.Food;
import food.Meat;

public class Herbivore extends Animal {
   public int satiety;

    @Override
    public void eat(Food food) {
        if (food instanceof Meat) {
            System.out.println("Травоядные животные не едят мясо");
        } else {
            System.out.println("Ем траву");
        }
    }


}
